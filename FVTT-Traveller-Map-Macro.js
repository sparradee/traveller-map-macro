/**
 * @author Rthr-X#8757
 * 
 * Patched 25-Dec-2021: Added a workaround for a bug in the current inline-webviewer module
 * 
 * IMPORTANT NOTES:
 *  1.  This macro requires the "Inline Webviewer" module by Ardittristan:
 *      https://github.com/ardittristan/VTTInlineWebviewer
 *  2.  This macro will let users post ChatMessages with buttons that link
 *      back to the map; THOSE BUTTONS WILL NOT WORK FOR OTHER PLAYERS UNTIL
 *      THOSE OTHER PLAYERS RUN THIS MACRO ONCE! The buttons have no built-in
 *      functionality, but when this macro is run the first time it sets up
 *      a watcher to add a click-handling function to the buttons in chat.
 *
 * The defaultOptions object lets you configure the following:
 *
 *  mapX, mapY, zoom        The default starting location for when the map opens.
 *                          e.g., -94, 25, 7.35 places Trojan Reach/Sindal fully in view.
 *                          Note that there is a difference between World X/Y coordinates
 *                          and Map X/Y coordinates -- see the TravellerMap API at:
 *                          https://travellermap.com/doc/api#coordinate-systems-the-varied-esoteric-xy-values
 *
 *  forceUI                 If true, put the whole TravellerMap UI on the window; if
 *                          false, just show the map.
 *
 *  mapOptions              TravellerMap display options; see the API for details.
 *
 *  windowSize              Width and Height of the browser window, in pixels.
 *
 *  useTravellerTools       If true, create a link to the UWP decoder at:
 *                          https://travellertoolsdemo.azurewebsites.net/
 *
 *  fetchSystemData         If true, fetch data from TravellerMap when you click on a
 *                          system. NOTE that this will put a brief delay between a
 *                          click and the confirmation window popping up; if the network
 *                          latency is too high, consider turning this off.
 */
 const defaultOptions = {
  mapX: -106.276,
  mapY: 51.431,
  zoom: 7.35,
  forceUI: false,
  mapOptions: 58367,
  windowSize: 900,
  useTravellerTools: true,
  fetchSystemData: true
}
const userId = game.user.id

/**
* Establish functions to clear previously created listeners, so there's
* only ever one running at once.  Hopefully.
*/
window._travellerMapListeners = window._travellerMapListeners || []
window._cleanupTravellerMapListeners =
  window._cleanupTravellerMapListeners ||
  function () {
      window._travellerMapListeners.forEach((close) => close())
  }
window._cleanupTravellerMapListeners()

/**
* Start a watcher that will add a click handler to any new buttons we've
* added in ChatMessages.
*/
window._travellerButtonWatcher =
  window._travellerButtonWatcher ||
  setInterval(() => {
      const buttons = $('.traveller-map-button')
      buttons.each((index, button) => {
          $(button).off('click') // Make sure there's only one handler at a time
          $(button).on('click', () => {
              const url = $(button).data('url')
              newWindow(url)
          })
      })
  }, 1500)

/**
* Opens a new Inline Webviewer to the specified url.
*
* @param {string} url
*/
const newWindow = (url) => Ardittristan.InlineViewer.sendUrl(url, false, defaultOptions.windowSize, defaultOptions.windowSize, '', '', [userId], {})

/**
* This class handles the core functionality of the macro.
*/
class TravellerMap {

  /**
   * An InlineViewer window object, from the 'Inline Webviewer' module.
   *
   * @var {object}

   */
  window = null

  /**
   * @var {object}

   */
  confirmDialog = null

  /**
   * Instantiate a new InlineViewer and activate a listener on it.
   *
   * @param {string} [url]
   */
  constructor (url) {
      this.createWindow(url || this.mapUrl())
  }

  /**
   * @param {string} [url]
   */
  createWindow (url) {
      this.window = newWindow(url || this.mapUrl())
      window.addEventListener('message', this.onMapMessage)
      window._travellerMapListeners.push(() =>
          window.removeEventListener('message', this.onMapMessage)
      )
  }

  /**
   * Construct a TravellerMap URL from the specified location. If no location is provided,
   * use the defaultOptions.
   *
   * @param {number} [x] Map X
   * @param {number} [y] Map Y
   * @param {number} [z] Zoom
   */
  mapUrl (
      x = defaultOptions.mapX,
      y = defaultOptions.mapY,
      z = defaultOptions.zoom
  ) {
      return [
          `https://travellermap.com/`,
          `?p=${x}!${y}!${z}`,
          `&options=${defaultOptions.mapOptions}`,
          `&forceui=${defaultOptions.forceUI}`,
      ].join('')
  }

  /**
   * When the TravellerMap receives a click, it will broadcasts a message to us; we catch
   * that message and put a handler on it, allowing a user to post a message to the
   * chat linking other users to the clicked location.
   *
   * If defaultOptions.fetchSystemData is true, we first query TravellerMap again for
   * information on the clicked system, so we can share it.
   *
   * @param {object} e The eventHandler received message
   * @param {string} e.origin The source of the message (for us, should be 'https://travellermap.com')
   * @param {object} e.data The message data package
   * @param {string} e.data.source Should be 'travellermap'
   * @param {string} e.data.type Event type one of 'click' or 'doubleclick'
   * @param {object} e.data.location
   * @param {number} e.data.location.x A Map X coordinate
   * @param {number} e.data.location.y A Map Y coordinate
   */
  onMapMessage (e) {
      if (!/travellermap/i.test(e.origin)) return

      // Make sure only one confirmation window opens up at a time
      if (this.confirmDialog && this.confirmDialog.rendered) {
          return
      }

      const self = this
      const loc = e.data.location // {x: {number}, y: {number}}
      let worldContent = ''

      /**
       * Quick function to open the confirmation dialog, with or without the system data.
       */
      function openDialog () {
          self.confirmDialog = new Dialog({
              title: 'Post Link in Chat?',
              content: `${worldContent}<p>Do you wish to post a shortcut to this location in chat?</p>`,
              buttons: {
                  confirm: {
                      label: 'Yes',
                      callback: async () => {
                          const loc = e.data.location // {x: {number}, y: {number}}
                          const mapLoc = worldXYToMapXY(loc.x, loc.y) // {x: {float}, y: {float}}
                          const url = MapWindow.mapUrl(mapLoc.x, mapLoc.y) // {string}

                          const newMessage = await ChatMessage.create({
                              user: userId,
                              content: `${worldContent}<button class='traveller-map-button' data-url='${url}'>TravellerMap Link</button>`,
                          })
                      },
                  },
                  deny: {
                      label: 'No',
                  },
              },
              default: 'deny',
          }).render(true)
      }

      if (!defaultOptions.fetchSystemData) {
          return openDialog()
      }


      fetch(`https://travellermap.com/api/jumpworlds?x=${loc.x}&y=${loc.y}&jump=0`).then((response) => {
          response.json().then((data) => {
              const worldData = data.Worlds[0]
              worldContent = [
                  `<h2>${worldData.Name}</h2>`,
                  `<ul>`,
                  `<li><b>Sector</b>: ${worldData.Sector} / ${worldData.SubsectorName}</li>`,
                  defaultOptions.useTravellerTools
                      ? `<li><b>UWP</b>: <a _target='blank' href='https://travellertoolsdemo.azurewebsites.net/uwp/${worldData.UWP}'>${worldData.UWP}</a></li>`
                      : `<li><b>UWP</b>: ${worldData.UWP}</li>`,
                  `<li><b>Stellar</b>: ${worldData.Stellar}</li>`,
                  `<li><b>Planets</b>: ${worldData.Worlds}</li>`,
                  `<li><b>Remarks</b>: ${worldData.Remarks}</li>`,
                  `<li><b>Allegiance</b>: ${worldData.AllegianceName}</li>`,
                  `</ul>`,
                  `<br />`
              ].join('')

              openDialog()
          })
      })
  }
}

/**
* Functions taken from the TravellerMap API for translating WorldXY coordinates
* into MapXY coordinates. Taken from the API at:
*  https://travellermap.com/doc/api#coordinate-systems-the-varied-esoteric-xy-values
*/
const isEven = (n) => n % 2 === 0
const PARSEC_SCALE_X = Math.cos(Math.PI / 6) // cosine 30Â°
const PARSEC_SCALE_Y = 1
function worldXYToMapXY (world_x, world_y) {
  const ix = world_x - 0.5
  const iy = isEven(world_x) ? world_y - 0.5 : world_y
  const x = ix * PARSEC_SCALE_X
  const y = iy * -PARSEC_SCALE_Y
  return { x, y }
}

// Patch a v9-related bug in inline-webviewer (25 Dec 2021)
game.user._id = game.user.id

// Last, but not least, we kick off the macro by opening the map!
const MapWindow = new TravellerMap()