# Traveller Map Macro

A Foundry VTT macro for use in the Traveller RPG. This macro leverages the Inline Webviewer (https://github.com/ardittristan/VTTInlineWebviewer) library and the https://travellermap.com website to provide GM's and players quick access to map tools.

## Instructions

Create a new script macro in Foundry, and copy all the contents of the `FVTT-Traveller-Map-Macro.js` file into that macro. Once it's added to Foundry's macro index, instruct all your players to place it on their hotbar.

### Caveat

The macro allows any one player to post a chat message linking to a specific system on TravellerMap, with a button to view that system - **This button will only work for other players who have run the macro once in this game session.** The macro needs to be run to set up a process that watches for those chat button and creates the needed functionality in order to view the shared content.
